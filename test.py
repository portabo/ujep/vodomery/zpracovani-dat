from datetime import datetime
from enum import Enum

from ujep_voda.models.datagram import DatagramFooter, KamstrupRecord, MaddalenaRecord
from ujep_voda.parsers import (
    get_datagram_footer_json,
    get_datagram_header_json,
    get_kamstrup_record_json,
    get_maddalena_record_json,
)
from ujep_voda.utils import check_header, parse_datagram

# datagram = "01013aa99090024040000000010"
# datagram = "68 81 81 68 08 5d 72 93 75 16 85 2d 2c 41 16 1e 00 00 00 04 13 30 64 06 00 84 40 14 00 00 00 00 84 80 40 14 00 00 00 00 04 22 f8 0b 00 00 34 22 ce 02 00 00 04 3c 00 00 00 00 14 3c 27 00 00 00 04 ff 22 00 00 00 00 04 6d 3b 33 e1 28 44 13 00 00 00 00 c4 40 14 00 00 00 00 c4 80 40 14 00 00 00 00 54 3c 00 00 00 00 42 6c 00 00 02 ff 1a 04 2a 0c 78 93 75 16 85 04 ff 16 e5 84 1e 00 04 ff 17 c9 ff 0e 01 82 16"
# hex = datagram.split()
# record = get_kamstrup_record_json(hex)
# print(record)
# record = KamstrupRecord(**record)
# print(record)


# header = get_datagram_header_json(datagram)
# header = MaddalenaHeader(**header)
# print(header)

# record = get_maddalena_record_json(datagram)
# record = MaddalenaRecord(**record)
# print(record)

# footer = get_datagram_footer_json(datagram)
# footer = DatagramFooter(**footer)
# print(footer)

# def parse_maddalena(dt: datetime, hex: str, id: str) -> MaddalenaDatagram:
#     status = [
#         int(x) for x in hex[16]
#     ]  # Warning Flags (bit 0 = busy, bit 1 = error, bit 2 = low battery)
#     start_const = 206  # Starting Constant
#     flow = (
#         int.from_bytes(bytearray.fromhex("".join(hex[35:39])), "little") + start_const
#     )

#     return MaddalenaDatagram(dt=dt, id=id, flow=flow, status=status)

# test = "1382520"
# print(test[-3:])
# print(test[:-3] + "." + test[-3:])
