## Poetry
FROM python:3.10-slim-bookworm as requirements-stage

WORKDIR /tmp

RUN pip install poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

## App
FROM python:3.10-slim-bookworm

# Zabraňuje vytváření pyc souborů (cache)
ENV PYTHONDONTWRITEBYTECODE 1

# Zabraňuje bufferování stdin/stdout
ENV PYTHONUNBUFFERED 1 

# Nastavení časového pásma

ENV TZ=Europe/Prague
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /code

COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./pyproject.toml /code/
COPY ./ujep_voda /code/ujep_voda
COPY ./main.py /code/

ENTRYPOINT ["python", "main.py"]
