import pathlib
import threading

from ujep_voda.database import create_db_and_tables, get_engine
from ujep_voda.logging import init_logger
from ujep_voda.models.mqtt_client import MqttClient
from ujep_voda.models.settings import Settings
from ujep_voda.utils import mqtt_parsing_posting

files_path = pathlib.Path(__file__).parent / "files"
log_path = files_path / "app.log"
database_path = files_path / "database.sqlite"

if not log_path.exists():
    log_path.parent.mkdir(parents=True, exist_ok=True)
    log_path.touch()

logger = init_logger("logger", log_path)  # Inicializace loggeru.
settings = Settings()  # Inicializace nastavení.

engine = get_engine(settings, database_path)
create_db_and_tables(engine)

mqtt = MqttClient(
    host=settings.mqtt.host,
    port=settings.mqtt.port,
    username=settings.mqtt.username,
    password=settings.mqtt.password.get_secret_value(),
    client_id=settings.mqtt.client_id,
    topics=settings.mqtt.topics,
)  # Inicializace MQTT klienta.

mqtt_forward = MqttClient(
    host=settings.mqtt_forward.host,
    port=settings.mqtt_forward.port,
    username=settings.mqtt_forward.username,
    password=settings.mqtt_forward.password.get_secret_value(),
    client_id=settings.mqtt_forward.client_id,
    topics=settings.mqtt_forward.topics,
)  # Inicializace MQTT klienta.

mqtt_forward.connect(secure=True)

mqtt.connect()
mqtt.client.loop_start()  # Začne odposlouchávat zprávy z MQTT brokeru v novém threadu.

mqtt_thread = threading.Thread(
    target=mqtt_parsing_posting, args=(settings, mqtt, mqtt_forward, engine)
)  # Vytvoří nový thread pro zpracování zpráv z MQTT.

mqtt_thread.start()
