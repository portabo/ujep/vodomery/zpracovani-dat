from enum import Enum
from urllib.parse import urljoin
from uuid import uuid4

from pydantic import BaseSettings, SecretStr, validator


class MqttSettings(BaseSettings):
    host: str
    port: int
    username: str
    password: SecretStr
    client_id: str | None
    topics: str | list[tuple[str, int]]

    class Config:
        env_file = ".env"
        case_sensitive = False
        env_file_encoding = "utf-8"
        env_prefix = "MQTT_"

    @validator("client_id")
    def generate_client_id(cls, v):
        if v is None:
            v = uuid4().hex
        return v

    @validator("topics")
    def add_topics(cls, v):
        if ";" in v:
            v = [(topic, 2) for topic in v.split(";")]
        else:
            v = [(v, 2)]
        return v


class MqttForward(MqttSettings):
    class Config:
        env_file = ".env"
        case_sensitive = False
        env_file_encoding = "utf-8"
        env_prefix = "FORWARD_MQTT_"


class TelegrafPorts(Enum):
    MADDALENA = 6000
    KAMSTRUP = 5000
    LORA_KAMSTRUP = 5000


class TelegrafSettings(BaseSettings):
    host: str
    path: str

    class Config:
        env_file = ".env"
        case_sensitive = False
        env_file_encoding = "utf-8"
        env_prefix = "TELEGRAF_"

    def get_url(self, datagram_type):
        datagram_type = datagram_type.upper()
        url = f"http://{self.host}:{TelegrafPorts[datagram_type].value}"

        return urljoin(url, self.path)


class Settings(BaseSettings):
    mqtt: MqttSettings = MqttSettings()
    mqtt_forward: MqttForward = MqttForward()
    telegraf: TelegrafSettings = TelegrafSettings()
    debug: bool

    class Config:
        env_file = ".env"
        case_sensitive = False
        env_file_encoding = "utf-8"
        env_prefix = "APP_"
