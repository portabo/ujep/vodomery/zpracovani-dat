import json
from datetime import datetime
from decimal import Decimal

from pydantic import BaseModel, StrictInt, root_validator, validator


class Measurement(BaseModel):
    value: StrictInt | Decimal | str
    unit: str | None


class DatagramHeader(BaseModel):
    start: str
    l_field: int
    c_field: str
    a_field: str
    ci_field: str
    sn: StrictInt | str
    manufacturer: str
    version: str
    medium: str
    status: str
    configuration: tuple[str, str]

    @root_validator(pre=True)
    def check_header(cls, values):
        if values.get("start") == values.get("start_1"):
            del values["start_1"]
        else:
            raise ValueError("start bytes not equal")

        # Toto je zbytečné, neboť již kontroluji dříve (check_header v utils.py)
        if values.get("l_field") == values.get("l_field_1"):
            values["l_field"] = int(values["l_field"], base=16)
            del values["l_field_1"]
        else:
            raise ValueError("l_field bytes not equal")

        if values.get("configuration") and values.get("configuration_1"):
            values["configuration"] = (
                values["configuration"],
                values["configuration_1"],
            )
            del values["configuration_1"]

        return values

    def to_influx(self, dump=True):
        header = {
            # "sn": self.sn,
            "status": self.status,
        }

        if dump:
            return json.dumps(header)

        return header


class MaddalenaRecord(BaseModel):
    water_meter_sn: str
    volume: Measurement
    back_flow_volume: Measurement
    forward_flow_rate: Measurement
    max_forward_flow_rate: Measurement
    max_back_flow_rate: Measurement
    alarms_registers: str
    firmware_version: str

    def to_influx(self, dump=True):
        record = {
            "volume": self.volume.value,  # Z litrů na m3
            "actual_flow": self.forward_flow_rate.value,
            "max_total_flow": self.max_forward_flow_rate.value,
            "errors": self.alarms_registers,
        }

        if dump:
            return json.dumps(record)

        return record

    @classmethod
    def _get_value_from_bytes(
        cls, values: list[str], byte_order: str = "little"
    ) -> int:
        return int.from_bytes(bytearray.fromhex("".join(values)), byte_order)

    @classmethod
    def _get_decimals(cls, vif: str):
        match vif:
            case "13":
                return (0, "l")
            case "93":
                return (0, "l")
            case "3b":
                return (0, "l/h")
            case "bb":
                return (0, "l/h")

    @classmethod
    def _transform_decimals(
        cls, value: int, decimals: int
    ) -> Decimal:  # TODO Zkontrolovat, možná je to špatně.
        if not decimals == 0:
            before_point = str(value)[:-decimals]
            after_point = str(value)[-decimals:]
            return Decimal(f"{before_point}.{after_point}")

        return int(value)

    @validator("firmware_version", pre=True)
    def check_firmware_version(cls, v):
        if v.get("value"):
            v["value"].reverse()
            v = ".".join(v["value"])
        return v

    @validator("alarms_registers", pre=True)
    def get_info_bits(cls, v):
        if v.get("value"):
            v = "".join(v["value"])
        return v

    @validator(
        "water_meter_sn",
        pre=True,
    )
    def get_sn(cls, v):
        if v.get("value"):
            return v["value"]
        else:
            raise ValueError(f"missing water_meter_sn")

    @validator(
        "volume",
        "back_flow_volume",
        "forward_flow_rate",
        "max_forward_flow_rate",
        "max_back_flow_rate",
        pre=True,
    )
    def check_measurements(cls, v, values, field):
        if v.get("value") and v.get("vif"):
            value = cls._get_value_from_bytes(v["value"])
            decimal_point, unit = cls._get_decimals(v["vif"])
            value = cls._transform_decimals(value, decimal_point)
            if field.name == "volume":
                value = (
                    value + 206
                )  # FIXME: Přičtení začínající hodnoty, nutné případně změnit!
            v = Measurement(value=value, unit=unit)
        else:
            raise ValueError(f"problem with {v}: {values.get(v)}")

        return v

    @validator(
        "back_flow_volume",
        "forward_flow_rate",
        "max_forward_flow_rate",
        "max_back_flow_rate",
    )
    def check_flow(cls, v: Measurement) -> Measurement:
        if v.value and v.unit:
            if v.unit == "l/h":
                v.value = v.value / 1000
                v.unit = "m3/h"

        return v

    @validator("volume")
    def check_volume(cls, v: Measurement) -> Measurement:
        if v.value and v.unit:
            if v.unit == "l":
                v.value = v.value / 1000
                v.unit = "m3"

        return v


class KamstrupRecord(BaseModel):
    volume_v1: Measurement
    volume_v1_year: Measurement
    pulse_input_a1: Measurement
    pulse_input_a1_year: Measurement
    pulse_input_b1: Measurement
    pulse_input_b1_year: Measurement
    flow_v1_actual: Measurement
    flow_v1_max_month: Measurement
    flow_v1_max_year: Measurement
    operating_hours: Measurement
    error_hour_counter: Measurement
    module_type_config_no: int
    module_sw_revision: int
    meter_type: str
    fabrication_number: Measurement
    info_bits: str
    date_year: str
    date_and_time: datetime

    def to_influx(self, dump=True):
        record = {
            "timestamp": self.date_and_time.timestamp(),
            "volume": float(self.volume_v1.value),
            "actual_flow": float(self.flow_v1_actual.value),
            "max_monthly_flow": float(self.flow_v1_max_month.value),
            "errors": self.info_bits,
        }

        if dump:
            return json.dumps(record)

        return record

    @classmethod
    def _get_value_from_bytes(
        cls, values: list[str], byte_order: str = "little"
    ) -> int:
        return int.from_bytes(bytearray.fromhex("".join(values)), byte_order)

    @classmethod
    def _get_meter_type(cls, hex: str) -> str:
        match hex:
            case "1a":
                return "MC403"
            case "1b":
                return "MC603"
            case "1c":
                return "MC803"
            case "1d":
                return "MC603M"
            case "1e":
                return "MC803M"

    @classmethod
    def _get_decimals(cls, vif: str):
        match vif:
            case "07":
                return (2, "MWh")
            case "13":
                return (3, "m3")
            case "14":
                return (2, "m3")
            case "15":
                return (1, "m3")
            case "16":
                return (0, "m3")
            case "22":
                return (0, "h")
            case "3b":
                return (0, "l/h")
            case "3c":
                return (2, "m3/h")
            case "3d":
                return (1, "m3/h")
            case "3e":
                return (0, "m3/h")
            case "ff22":
                return (0, None)
            case "ff1a":
                return (0, None)
            case "ff16":
                return (0, "number")
            case "fd0e":
                return (0, "number")
            case "6c":
                return (0, None)
            case "6d":
                return (0, None)
            case "78":
                return (0, "number")

    @classmethod
    def _transform_decimals(
        cls, value: int, decimals: int
    ) -> Decimal:  # TODO Zkontrolovat, možná je to špatně.
        if not decimals == 0:
            before_point = str(value)[:-decimals]
            after_point = str(value)[-decimals:]
            return Decimal(f"{before_point}.{after_point}")

        return int(value)

    @classmethod
    def _transform_millenium(cls, millenium: str) -> int:
        match millenium:
            case "00":
                return 1900
            case "01":
                return 2000
            case "10":
                return 2100
            case "11":
                return 2200

    @classmethod
    def _get_binary(cls, hex: str) -> str:
        return "{0:08b}".format(int(hex, 16))

    @classmethod
    def _get_datetime(cls, hexes: list[str]) -> datetime:
        bin = []
        for hex in hexes:
            bin.append(cls._get_binary(hex))

        bin_0 = bin[0]
        bin_1 = bin[1]
        bin_2 = bin[2]
        bin_3 = bin[3]

        valid = True if int(bin_0[0], 2) == 0 else False
        minute = int(bin_0[2:], 2)

        summer_time = (
            True if int(bin_1[0], 2) == 1 else False
        )  # TODO: S tímto není pracováno, asi ani není nastaveno na zařízení
        millenium = cls._transform_millenium(bin_1[1] + bin_1[2])
        hour = int(bin_1[3:], 2)
        year = millenium + int(bin_3[0:4] + bin_2[0:3], 2)
        day = int(bin_2[3:], 2)
        month = int(bin_3[4:], 2)

        if valid:
            return datetime(year, month, day, hour, minute)

    @classmethod
    def _get_date(cls, hexes: list[str]) -> datetime:
        bin = []
        for hex in hexes:
            bin.append(cls._get_binary(hex))

        bin_0 = bin[0]
        bin_1 = bin[1]

        year = int(bin_1[0:4] + bin_0[0:3], 2)
        day = int(bin_0[3:], 2)
        month = int(bin_1[5:], 2)

        return f"{year}-{month}-{day}"

    @validator(
        "volume_v1",
        "volume_v1_year",
        "pulse_input_a1",
        "pulse_input_a1_year",
        "pulse_input_b1",
        "pulse_input_b1_year",
        "flow_v1_actual",
        "flow_v1_max_month",
        "flow_v1_max_year",
        "operating_hours",
        "error_hour_counter",
        "fabrication_number",
        pre=True,
    )
    def check_measurements(cls, v, values):
        if v.get("value") and v.get("vif"):
            value = cls._get_value_from_bytes(v["value"])
            decimal_point, unit = cls._get_decimals(v["vif"])
            value = cls._transform_decimals(value, decimal_point)
            v = Measurement(value=value, unit=unit)
        else:
            raise ValueError(f"problem with {v}: {values.get(v)}")

        return v

    @validator("module_type_config_no", "module_sw_revision", pre=True)
    def check_bytes(cls, v):
        if v.get("value"):
            v = cls._get_value_from_bytes(v["value"])
        return v

    @validator("info_bits", pre=True)
    def get_info_bits(cls, v):
        if v.get("value"):
            v = "".join(v["value"])
        return v

    @validator("meter_type", pre=True)
    def check_meter_type(cls, v):
        if v.get("vif"):
            v = cls._get_meter_type(v["vif"][2:])
        return v

    @validator("date_and_time", pre=True)
    def check_date_and_time(cls, v):
        if v.get("value"):
            v = cls._get_datetime(v["value"])
        return v

    @validator("date_year", pre=True)
    def check_date(cls, v):
        if v.get("value"):
            v = cls._get_date(v["value"])
        return v

    @validator(
        "flow_v1_actual",
        "flow_v1_max_month",
        "flow_v1_max_year",
    )
    def check_flow(cls, v: Measurement) -> Measurement:
        if v.value and v.unit:
            if v.unit == "l/h":
                v.value = v.value / 1000
                v.unit = "m3/h"

        return v


class DatagramFooter(BaseModel):
    checksum: str
    stop: str

    @validator("stop", pre=True)
    def check_stop(cls, v):
        if v == "16":
            return v
        else:
            raise ValueError("stop byte not equal to 16")


class Datagram(BaseModel):
    id: str
    type: str
    received: datetime
    header: DatagramHeader
    record: MaddalenaRecord | KamstrupRecord
    footer: DatagramFooter

    def to_influx(self, dump=True):
        datagram = {
            "id": self.id,
            "received": self.received.timestamp(),
            # "type": self.type,
        }
        datagram = (
            datagram
            | self.header.to_influx(dump=False)
            | self.record.to_influx(dump=False)
        )

        if dump:
            return json.dumps(datagram)

        return datagram


class LoraRecord(BaseModel):
    id: str
    type: str
    received: datetime
    status: str | None
    volume: Measurement
    flow: Measurement | None
    flow_max_month: Measurement | None
    info_bits: str | None

    def to_influx(self, dump=True):
        record = {
            "id": self.id,
            # "sn": None,
            "status": self.status,
            "timestamp": None,
            "received": self.received.timestamp(),
            "volume": float(self.volume.value),
            "actual_flow": float(self.flow.value) if self.flow else None,
            "max_monthly_flow": float(self.flow_max_month.value)
            if self.flow_max_month
            else None,
            "errors": self.info_bits,
        }

        if dump:
            return json.dumps(record)

        return record

    @classmethod
    def _get_decimals(cls, vif: str):
        match vif:
            case "07":
                return (2, "MWh")
            case "13":
                return (3, "m3")
            case "14":
                return (2, "m3")
            case "15":
                return (1, "m3")
            case "16":
                return (0, "m3")
            case "22":
                return (0, "h")
            case "3b":
                return (0, "l/h")
            case "3c":
                return (2, "m3/h")
            case "3d":
                return (1, "m3/h")
            case "3e":
                return (0, "m3/h")
            case "ff22":
                return (0, None)
            case "ff1a":
                return (0, None)
            case "ff16":
                return (0, "number")
            case "fd0e":
                return (0, "number")
            case "6c":
                return (0, None)
            case "6d":
                return (0, None)
            case "78":
                return (0, "number")

    @classmethod
    def _transform_decimals(
        cls, value: int, decimals: int
    ) -> Decimal:  # TODO Zkontrolovat, možná je to špatně.
        if not decimals == 0:
            before_point = str(value)[:-decimals]
            after_point = str(value)[-decimals:]
            return Decimal(f"{before_point}.{after_point}")

        return int(value)

    @classmethod
    def _get_value_from_bytes(cls, values: str, byte_order: str = "little") -> int:
        return int.from_bytes(bytearray.fromhex(values), byte_order)

    @validator("info_bits", pre=True)
    def get_info_bits(cls, v):
        if v.get("value"):
            v = v["value"]
        return v

    @validator(
        "volume",
        "flow",
        "flow_max_month",
        pre=True,
    )
    def check_measurements(cls, v, values):
        if v.get("value") and v.get("vif"):
            value = cls._get_value_from_bytes(v["value"])
            decimal_point, unit = cls._get_decimals(v["vif"])
            value = cls._transform_decimals(value, decimal_point)
            v = Measurement(value=value, unit=unit)
        else:
            raise ValueError(f"problem with {v}: {values.get(v)}")

        return v

    @validator(
        "flow",
        "flow_max_month",
    )
    def check_flow(cls, v: Measurement) -> Measurement:
        if v.value and v.unit:
            if v.unit == "l/h":
                v.value = v.value / 1000
                v.unit = "m3/h"

        return v
