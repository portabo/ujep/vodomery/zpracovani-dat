import logging
import ssl
from datetime import datetime
from queue import Queue
from uuid import UUID, uuid4

import paho.mqtt.client as mqtt
from pydantic import BaseModel, Field, SecretStr

logger = logging.getLogger("logger")
log_prefix = "MQTT |"


class MqttMessage(BaseModel):
    id: UUID = Field(default_factory=uuid4)
    dt: datetime
    topic: str
    payload: str


class MqttClient(BaseModel):
    host: str
    port: int
    username: str
    password: SecretStr
    client_id: str
    userdata: dict | None
    protocol: int = 5
    topics: list[tuple[str, int]]
    queue: Queue = Field(default_factory=Queue)
    client: mqtt.Client | None

    class Config:
        arbitrary_types_allowed = True

    def connect(self, secure: bool = False):
        client = mqtt.Client(self.client_id, self.userdata, self.protocol)
        client.username_pw_set(self.username, self.password.get_secret_value())
        if secure:
            client.tls_set(
                "/etc/ssl/certs/ca-certificates.crt", tls_version=ssl.PROTOCOL_TLSv1_2
            )

        client.connect(self.host, self.port)
        client.on_connect = self._on_connect
        client.on_disconnect = self._on_disconnect
        client.on_subscribe = self._on_subscribe
        client.on_message = self._on_message

        self.client = client

    def _parse_message(self, message: mqtt.MQTTMessage):
        message = MqttMessage(
            dt=datetime.now(),
            topic=message.topic,
            payload=message.payload.decode("utf-8"),
        )
        return message

    def _on_connect(self, client, userdata, flags, rc):
        logger.info(f"{log_prefix} Connected with code {rc}")
        client.subscribe(self.topics)

    def _on_disconnect(self, client, userdata, rc):
        logger.warning(f"{log_prefix} Disconnected with code {rc}")

    def _on_subscribe(self, client, userdata, mid, granted_qos):
        topics = ", ".join([topic[0] for topic in self.topics])
        logger.info(f"{log_prefix} Subscribed to topics: {topics}")
        logger.info(f"{log_prefix} Waiting for messages...")

    def _on_message(self, client, userdata, message):
        message = self._parse_message(message)
        self.queue.put(message)
        logger.info(
            f'{log_prefix} Queue: {self.queue.qsize()} | {message.json(exclude={"payload"})}'
        )
