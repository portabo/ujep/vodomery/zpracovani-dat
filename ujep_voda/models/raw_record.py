from datetime import datetime

from sqlmodel import TEXT, Column, Field, SQLModel


class RawRecord(SQLModel, table=True):
    __tablename__ = "raw_records"

    id: int = Field(default=None, primary_key=True)
    topic: str = Field(index=True)
    received: datetime = Field(index=True)
    datagram: str = Field(sa_column=Column(TEXT))
