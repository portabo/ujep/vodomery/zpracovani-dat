from sqlmodel import SQLModel, create_engine


def get_engine(settings, file):
    sqlite_url = f"sqlite:///{file}"
    connect_args = {"check_same_thread": False}

    engine = create_engine(sqlite_url, echo=settings.debug, connect_args=connect_args)

    return engine


def create_db_and_tables(engine):
    SQLModel.metadata.create_all(engine)
