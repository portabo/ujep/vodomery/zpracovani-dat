import logging
from datetime import datetime
from typing import Any

import requests
from sqlmodel import Session

from .models.datagram import Datagram, KamstrupRecord, LoraRecord, MaddalenaRecord
from .models.mqtt_client import MqttClient, MqttMessage
from .models.raw_record import RawRecord
from .models.settings import Settings
from .parsers import (
    TOPIC_LORA_KAMSTRUP,
    TOPIC_MADDALENA,
    get_datagram_footer_json,
    get_datagram_header_json,
    get_kamstrup_record_json,
    get_lora_record_json,
    get_maddalena_record_json,
)

logger = logging.getLogger("logger")


def split_topic(topic: str) -> str:
    return topic.split("/")[-1]


def parse_message(message: MqttMessage) -> Datagram | None:
    log_prefix = "PARSE |"
    datagram = message.payload
    dt = message.dt
    id = split_topic(message.topic)

    try:
        if id in TOPIC_LORA_KAMSTRUP:
            try:
                parser = "lora_kamstrup"
                record = parse_datagram(parser, dt, datagram, id)
                return record

            except Exception as e:
                logger.error(f"{log_prefix} {e}")
                pass
        else:
            hex = datagram.split()
            header = check_header(hex)

            if header:
                if id in TOPIC_MADDALENA:
                    parser = "maddalena"
                else:
                    parser = "kamstrup"

                record = parse_datagram(parser, dt, hex, id)
                logger.info(
                    f'{log_prefix} {parser} | {record.json(exclude={"header","record", "footer"})}'
                )
                return record
            else:
                logger.error(
                    f"{log_prefix} {parser} | Invalid datagram header | {message.json(exclude={'payload'})}"
                )
                return None
    except Exception as e:
        logger.error(f"{log_prefix} {e}")
        pass

    return None


def save_raw_record(message: MqttMessage, engine):
    log_prefix = "SAVE |"

    try:
        datagram = message.payload
        dt = message.dt
        topic = message.topic

        raw_record = RawRecord(topic=topic, received=dt, datagram=datagram)

        with Session(engine) as session:
            session.add(raw_record)
            session.commit()
            session.refresh(raw_record)

        logger.info(f"{log_prefix} {raw_record.json(exclude={'datagram'})}")

    except Exception as e:
        logger.error(f"{log_prefix} {e}")
        pass


def forward_message(mqtt_forward: MqttClient, message: dict[str, Any], topic: str):
    log_prefix = f"FORWARD | {topic} |"

    try:
        mqtt_forward.client.publish(topic, message)
        logger.info(f"{log_prefix} {message}")
    except Exception as e:
        logger.error(f"{log_prefix} {e}")


def mqtt_parsing_posting(
    settings: Settings, mqtt: MqttClient, mqtt_forward: MqttClient, engine
):
    log_prefix = "POST |"

    while True:
        message = mqtt.queue.get()
        if message is None:
            continue

        try:
            save_raw_record(message, engine)
            record = parse_message(message)
            influx_record = record.to_influx()

            # FORWARD TO DCUK
            forward_message(
                mqtt_forward,
                message.json(),
                topic=f"{settings.mqtt_forward.topics[0][0]}raw",
            )
            forward_message(
                mqtt_forward,
                influx_record,
                topic=f"{settings.mqtt_forward.topics[0][0]}parsed",
            )

            url = settings.telegraf.get_url(record.type)
            response = requests.post(url=url, data=influx_record)
            log_text = f"{log_prefix} Response: {response.status_code} | Queue: {mqtt.queue.qsize()} | {influx_record}"
            if response.status_code == 204:
                logger.info(log_text)
            else:
                logger.error(log_text)

        except Exception as e:
            logger.error(f"{log_prefix} {e}")
            continue

        logger.info(f"MQTT | Waiting for messages...")


def parse_datagram(
    datagram_type: str, dt: datetime, hex: str | list[str], id: str
) -> Datagram:
    match datagram_type:
        case "maddalena":
            record = get_maddalena_record_json(hex)
            record = MaddalenaRecord(**record)
        case "kamstrup":
            record = get_kamstrup_record_json(hex)
            record = KamstrupRecord(**record)
        case "lora_kamstrup":
            record = get_lora_record_json(hex)
            record.update({"type": datagram_type, "received": dt, "id": id})
            return LoraRecord(**record)

    header = get_datagram_header_json(hex)
    footer = get_datagram_footer_json(hex)

    return Datagram(
        id=id,
        received=dt,
        type=datagram_type,
        header=header,
        record=record,
        footer=footer,
    )


def check_header(hex: list[str]) -> bool:
    start_field = True if hex[0] == hex[3] else False  # Start Message
    l_field = int(hex[1], base=16) == int(hex[2], base=16)
    end_field = True if int(hex[-1]) == 16 else False  # End of Message

    if start_field and l_field and end_field:
        return True
    else:
        return False
