import logging


def init_logger(name: str, file: str, level: int = logging.WARNING) -> logging.Logger:
    logger = logging.getLogger(name)
    logger.setLevel(
        logging.DEBUG
    )  # Nutné nastavit výchozí úroveň logování, v opačném případě všechny handleři budou mít úroveň WARNING.
    format = logging.Formatter(
        "%(levelname)s | %(asctime)s | %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S%z",
    )

    f_handler = logging.FileHandler(file)
    f_handler.setLevel(level)
    f_handler.setFormatter(format)

    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)
    c_handler.setFormatter(format)

    logger.addHandler(f_handler)
    logger.addHandler(c_handler)

    return logger
