TOPIC_MADDALENA = ["kampus_h"]
TOPIC_LORA_KAMSTRUP = ["klisska_28", "klisska_30", "kampus_t", "ff_b"]
# TOPIC_KAMSTRUP =


def get_datagram_header_json(datagram: str) -> dict:
    return {
        "start": datagram[0],
        "l_field": datagram[1],
        "l_field_1": datagram[2],
        "start_1": datagram[3],
        "c_field": datagram[4],
        "a_field": datagram[5],
        "ci_field": datagram[6],
        "sn": datagram[10] + datagram[9] + datagram[8] + datagram[7],
        "manufacturer": datagram[11] + datagram[12],
        "version": datagram[13],
        "medium": datagram[14],
        "access": datagram[15],
        "status": datagram[16],
        "configuration": datagram[17],
        "configuration_1": datagram[18],
    }


def get_datagram_footer_json(datagram: list[str]) -> dict:
    return {"checksum": datagram[-2], "stop": datagram[-1]}


def get_maddalena_record_json(datagram: list[str]) -> dict:
    return {
        "water_meter_sn": {
            "dif": datagram[19],
            "vif": datagram[20],
            "value": datagram[26]
            + datagram[25]
            + datagram[24]
            + datagram[23]
            + datagram[22]
            + datagram[21],
        },
        "date_and_time": {
            "dif": datagram[27],
            "vif": datagram[28],
            "value": datagram[29:33],
        },
        "volume": {
            "dif": datagram[33],
            "vif": datagram[34],
            "value": datagram[35:39],
        },
        "back_flow_volume": {
            "dif": datagram[39],
            "vif": datagram[40],
            "vife": datagram[41],
            "value": datagram[42:46],
        },
        "forward_flow_rate": {
            "dif": datagram[46],
            "vif": datagram[47],
            "value": datagram[48:51],
        },
        "periodic_reading_volume_t1": {
            "dif": datagram[51],
            "vif": datagram[52],
            "value": datagram[53:57],
        },
        "periodic_reading_date": {
            "dif": datagram[57],
            "vif": datagram[58],
            "value": datagram[59:61],
        },
        "monthly_reading_volume_t2": {
            "dif": datagram[61],
            "dife": datagram[62],
            "vif": datagram[63],
            "value": datagram[64:68],
        },
        "monthly_reading_date_t2": {
            "dif": datagram[68],
            "dife": datagram[69],
            "vif": datagram[70],
            "value": datagram[71:73],
        },
        "monthly_reading_volume_t3": {
            "dif": datagram[73],
            "dife": datagram[74],
            "vif": datagram[75],
            "value": datagram[76:80],
        },
        "monthly_reading_date_t3": {
            "dif": datagram[80],
            "dife": datagram[81],
            "vif": datagram[82],
            "value": datagram[83:85],
        },
        "monthly_reading_volume_t4": {
            "dif": datagram[85],
            "dife": datagram[86],
            "vif": datagram[87],
            "value": datagram[88:92],
        },
        "monthly_reading_date_t4": {
            "dif": datagram[92],
            "dife": datagram[93],
            "vif": datagram[94],
            "value": datagram[95:97],
        },
        "monthly_reading_volume_t5": {
            "dif": datagram[97],
            "dife": datagram[98],
            "vif": datagram[99],
            "value": datagram[100:104],
        },
        "monthly_reading_date_t5": {
            "dif": datagram[104],
            "dife": datagram[105],
            "vif": datagram[106],
            "value": datagram[107:109],
        },
        "monthly_reading_volume_t6": {
            "dif": datagram[109],
            "dife": datagram[110],
            "vif": datagram[111],
            "value": datagram[112:116],
        },
        "monthly_reading_date_t6": {
            "dif": datagram[116],
            "dife": datagram[117],
            "vif": datagram[118],
            "value": datagram[119:121],
        },
        "monthly_reading_volume_t7": {
            "dif": datagram[121],
            "dife": datagram[122],
            "vif": datagram[123],
            "value": datagram[124:128],
        },
        "monthly_reading_date_t7": {
            "dif": datagram[128],
            "dife": datagram[129],
            "vif": datagram[130],
            "value": datagram[131:133],
        },
        "monthly_reading_volume_t8": {
            "dif": datagram[133],
            "dife": datagram[134],
            "vif": datagram[135],
            "value": datagram[136:140],
        },
        "monthly_reading_date_t8": {
            "dif": datagram[140],
            "dife": datagram[141],
            "vif": datagram[142],
            "value": datagram[143:145],
        },
        "monthly_reading_volume_t9": {
            "dif": datagram[145],
            "dife": datagram[146],
            "vif": datagram[147],
            "value": datagram[148:152],
        },
        "monthly_reading_date_t9": {
            "dif": datagram[152],
            "dife": datagram[153],
            "vif": datagram[154],
            "value": datagram[155:157],
        },
        "monthly_reading_volume_t10": {
            "dif": datagram[157],
            "dife": datagram[158],
            "vif": datagram[159],
            "value": datagram[160:164],
        },
        "monthly_reading_date_t10": {
            "dif": datagram[164],
            "dife": datagram[165],
            "vif": datagram[166],
            "value": datagram[167:169],
        },
        "monthly_reading_volume_t11": {
            "dif": datagram[169],
            "dife": datagram[170],
            "vif": datagram[171],
            "value": datagram[172:176],
        },
        "monthly_reading_date_t11": {
            "dif": datagram[176],
            "dife": datagram[177],
            "vif": datagram[178],
            "value": datagram[179:181],
        },
        "monthly_reading_volume_t12": {
            "dif": datagram[181],
            "dife": datagram[182],
            "vif": datagram[183],
            "value": datagram[184:188],
        },
        "monthly_reading_date_t12": {
            "dif": datagram[188],
            "dife": datagram[189],
            "vif": datagram[190],
            "value": datagram[191:193],
        },
        "monthly_reading_volume_t13": {
            "dif": datagram[193],
            "dife": datagram[194],
            "vif": datagram[195],
            "value": datagram[196:200],
        },
        "monthly_reading_date_t13": {
            "dif": datagram[200],
            "dife": datagram[201],
            "vif": datagram[202],
            "value": datagram[203:205],
        },
        "max_forward_flow_rate": {
            "dif": datagram[205],
            "dife": datagram[206],
            "vif": datagram[207],
            "value": datagram[208:211],
        },
        "max_forward_flow_rate_date_and_time": {
            "dif": datagram[211],
            "dife": datagram[212],
            "vif": datagram[213],
            "value": datagram[214:216],
        },
        "max_back_flow_rate": {
            "dif": datagram[216],
            "dife": datagram[217],
            "vif": datagram[218],
            "vife": datagram[219],
            "value": datagram[220:223],
        },
        "max_back_flow_rate_date_and_time": {
            "dif": datagram[223],
            "dife": datagram[224],
            "vif": datagram[225],
            "value": datagram[226:228],
        },
        "day_month_of_next_periodic_reading": {
            "dif": datagram[228],
            "vif": datagram[229],
            "vife": datagram[230],
            "value": datagram[231:233],
        },
        "periodicity_in_months_of_periodic_reading": {
            "dif": datagram[233],
            "vif": datagram[234],
            "vife": datagram[235],
            "value": datagram[236:237],
        },
        "alarms_registers": {
            "dif": datagram[237],
            "vif": datagram[238],
            "vife": datagram[239],
            "value": datagram[240:242],
        },
        "firmware_version": {
            "dif": datagram[242],
            "vif": datagram[243],
            "vife": datagram[244],
            "value": datagram[245:247],
        },
        "firmware_checksum": {
            "dif": datagram[247],
            "vif": datagram[248],
            "vife": datagram[249],
            "value": datagram[250:252],
        },
    }


def get_kamstrup_record_json(datagram: list[str]) -> dict:
    return {
        "volume_v1": {
            "dif": datagram[19],
            "vif": datagram[20],
            "value": datagram[21:25],
        },
        "pulse_input_a1": {
            "dif": datagram[25] + datagram[26],
            "vif": datagram[27],
            "value": datagram[28:32],
        },
        "pulse_input_b1": {
            "dif": datagram[32] + datagram[33] + datagram[34],
            "vif": datagram[35],
            "value": datagram[36:40],
        },
        "operating_hours": {
            "dif": datagram[40],
            "vif": datagram[41],
            "value": datagram[42:46],
        },
        "error_hour_counter": {
            "dif": datagram[46],
            "vif": datagram[47],
            "value": datagram[48:52],
        },
        "flow_v1_actual": {
            "dif": datagram[52],
            "vif": datagram[53],
            "value": datagram[54:58],
        },
        "flow_v1_max_month": {
            "dif": datagram[58],
            "vif": datagram[59],
            "value": datagram[60:64],
        },
        "info_bits": {
            "dif": datagram[64],
            "vif": datagram[65] + datagram[66],
            "value": datagram[67:71],
        },
        "date_and_time": {
            "dif": datagram[71],
            "vif": datagram[72],
            "value": datagram[73:77],
        },
        "volume_v1_year": {
            "dif": datagram[77],
            "vif": datagram[78],
            "value": datagram[79:83],
        },
        "pulse_input_a1_year": {
            "dif": datagram[83] + datagram[84],
            "vif": datagram[85],
            "value": datagram[86:90],
        },
        "pulse_input_b1_year": {
            "dif": datagram[90] + datagram[91] + datagram[92],
            "vif": datagram[93],
            "value": datagram[94:98],
        },
        "flow_v1_max_year": {
            "dif": datagram[98],
            "vif": datagram[99],
            "value": datagram[100:104],
        },
        "date_year": {
            "dif": datagram[104],
            "vif": datagram[105],
            "value": datagram[106:108],
        },
        "meter_type": {
            "dif": datagram[108],
            "vif": datagram[109] + datagram[110],
            "value": datagram[111:113],
        },
        "fabrication_number": {
            "dif": datagram[113],
            "vif": datagram[114],
            "value": datagram[115]
            + datagram[116]
            + datagram[117]
            + datagram[118],  # BCD8
        },
        "module_type_config_no": {
            "dif": datagram[119],
            "vif": datagram[120] + datagram[121],
            "value": datagram[122:126],
        },
        "module_sw_revision": {
            "dif": datagram[126],
            "vif": datagram[127] + datagram[128],
            "value": datagram[129:133],
        },
    }


def get_lora_record_json(datagram: str) -> dict:
    datagram_length = len(datagram)

    match datagram_length:
        case 11:
            record = {
                # "id": datagram[0],
                "volume": {
                    "vif": datagram[1:3],
                    "value": datagram[3:11],
                },
            }
        case 17:
            record = {
                # "id": datagram[0],
                "volume": {
                    "vif": datagram[1:3],
                    "value": datagram[3:11],
                },
                "flow": {
                    "vif": "3b",
                    "value": datagram[11:17],
                },
            }
        case 19:
            record = {
                # "id": datagram[0],
                "volume": {
                    "vif": datagram[1:3],
                    "value": datagram[3:11],
                },
                "flow_max_month": {
                    "vif": "3b",
                    "value": datagram[11:19],
                },
            }
        case 25:
            record = {
                # "id": datagram[0],
                "volume": {
                    "vif": datagram[1:3],
                    "value": datagram[3:11],
                },
                "flow": {
                    "vif": "3b",
                    "value": datagram[11:17],
                },
                "flow_max_month": {
                    "vif": "3b",
                    "value": datagram[17:25],
                },
            }
        case 27:
            record = {
                # "id": datagram[0],
                "status": datagram[1:3],
                "volume": {
                    "vif": datagram[3:5],
                    "value": datagram[5:13],
                },
                "flow": {
                    "vif": "3b",
                    "value": datagram[13:19],
                },
                "info_bits": {
                    "vif": "ff22",
                    "value": datagram[19:27],
                },
            }

    return record
